package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIsPalindromeRegular() {
		assertTrue("Wrong", Palindrome.isPalindrome("anna"));
	}	
	
	@Test
	public void testIsPalindromeExceptional() {
		assertFalse("Wrong", Palindrome.isPalindrome("wanna"));
	}	
	
	@Test
	public void testIsPalindromeBoundaryIn() {
		assertTrue("Wrong", Palindrome.isPalindrome("\na\n"));
	}	
	
	@Test
	public void testIsPalindromeBoundaryOut() {
		assertFalse("Wrong", Palindrome.isPalindrome("a\n"));
	}

}
