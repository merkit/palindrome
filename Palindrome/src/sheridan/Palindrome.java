package sheridan;

public class Palindrome {

	public static boolean isPalindrome(String input) {
		input = input.toLowerCase().replaceAll(" ", "");
		
		for (int i = 0, j = input.length()-1; i < j; i++, j--) {
			if (input.charAt(i) != input.charAt(j)) {
				return false;
			}
		}
		
		return true;
	}
	
	public static void main(String[] args) {
		System.out.println("Is anna a palindrome: " + Palindrome.isPalindrome("anna"));
		System.out.println("Is raceCar a palindrome: " + Palindrome.isPalindrome("raceCar"));
		System.out.println("Is taco cat a palindrome: " + Palindrome.isPalindrome("taco cat"));
		System.out.println("Is hello a palindrome: " + Palindrome.isPalindrome("hello"));

	}

}
